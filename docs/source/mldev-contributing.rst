Contribution Guidelines
=======================

Please check the `CONTRIBUTING.md <https://gitlab.com/mlrep/mldev/-/blob/develop/CONTRIBUTING.md>`_
guide if you'd like to participate in the project, ask a question or give a suggestion.

General Guidelines
------------------

This guide helps you contribute to the MLdev project. We are going to mention
all contributors in the `project roster <https://gitlab.com/mlrep/mldev/-/blob/develop/NOTICE.md>`_
Please, also check the project license and overall `contribution guidelines <https://gitlab.com/mlrep/mldev/-/blob/develop/CONTRIBUTING.md>`_.

If you are parcitipating in an open source project for the first time
you may find the following useful:

 - A guide on how to participate in open source projects (in Russian) `<https://ru.hexlet.io/blog/posts/participate-in-open-source>`_
 - GitLab Flow process with Merge Requests / Pull Requests `<https://docs.gitlab.com/ee/topics/gitlab_flow.html>`_
 - How to use GitLab `<https://docs.gitlab.com/ee/gitlab-basics/>`_

Pre-requisites
--------------

Please, use your registered account wth GitLab.
Then you should be able to fork the project and then submit a Merge Request
or request access via standard Gitlab features.

Translating and contributing docs and tutorials
-----------------------------------------------

English translation and new project documentation can be added to the `Wiki <https://gitlab.com/mlrep/mldev/-/wikis>`_.
Please, check the following:

 - if English translation, file name should be in English
 - add a link to the original document so that it could be properly linked
 - add an issue here and describe your contribution


GitLab wiki is a Git repo, you may want to create a branch for your contribution and use offline editing tools

Reporting bugs and code fixes
-----------------------------

Please, start with `submitting an issue here <https://gitlab.com/mlrep/mldev/-/issues>`_.
In the issue, describe the enhancement or the bug you are providing a fix for:

 - is this a bug or enhancement?
 - brief description of the problem
 - actual vs expected behavior
 - proposed solution, may be a code patch or description

If you'd like to make a contribution to the project,
please send the access request to the MLDev project.

Joining the project
-------------------

We welcome everyone who'd like to contribute to the project.
Please, contact the developers at `<https://t.me/mldev_betatest>`_
Check also the list of issues waiting for contribution.