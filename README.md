# MLDev software

This repository contains the MLDev software, that facilitates running data science experiments, 
help in results presentation and eases paper preparation for students, data scientists and researchers.

The MLDev software provides the following features to help with automating machine learning experiments:
 - Configure stages and parameters of a data science experiment separately from your code
 - Conduct a repeatable experiment in Google Colab or PaperSpace
 - Keep versions of your code, results and intermediate files on Google Drive (other repos coming soon)
 - Use a variety of pre-built templates to get started: see [template-default](../../../../template-default) 
 and [template-intelligent-systems](../../../../template-intelligent-systems)

MLDev also provides some services that run alongside your experiment code:
You can have the notifications via Telegram about the exeptions while training your model
 - Keep updated on current experiment state using TensorBoard (even on Colab)
 - Deploy and demo your model with a model controller (feature in progress) 

# Quick setup

Get the latest version of our install file to your local machine and run it.

```shell script
$ curl https://gitlab.com/mlrep/mldev/-/raw/develop/install_mldev.sh -o install_mldev.sh 
$ chmod +x ./install_mldev.sh
$ ./install_mldev.sh base
```
You may be asked for ``root`` privileges if there are [system packages to be installed](#install-system-packages)

Wait a couple of minutes until installation will be done and then you are almost ready to use our instrument, congrats!

Then get the default demo experiment

```shell script
$ mldev init <new_folder>
```

Answer the questions the setup wizard asks or skip where possible.

Then run the default pipeline of the experiment

```shell script
$ cd <new_folder>
$ mldev run
```

# Tutorial

A [Quick start tutorial](../../wikis/mldev-tutorial-basic) to get familiar with MLDev is available [here](../../wikis/mldev-tutorial-basic)

# User Guide

A [User guide](../../wikis/mldev-user-guide) is available on the project wiki [here](../../wikis/mldev-user-guide).
    
# Contributing

Please check the [CONTRIBUTING.md](CONTRIBUTIONG.md) guide if you'd like to participate in the project, ask a question or give a suggestion.

# Project sponsors and supporters

<p><a href="https://fund.mipt.ru"><img height="50px" src="../../wikis/images/fund-logo.svg" alt="MIPT Fund"/></a>
<a href="https://mipt.ru"><img src="https://mipt.ru/docs/download.php?code=mipt_eng_base_png" alt="MIPT" height="100px"/></a>
<a href="https://mipt.ru/education/departments/fpmi/"><img src="https://mipt.ru/docs/download.php?code=logotip_fpmi_2019" height="100px" alt="FPMI"/></a>
<a href="http://www.machinelearning.ru"><img src="http://www.machinelearning.ru/wiki/logo.png" alt="MachineLearning.ru" height="120px"/></a>
<a href="http://m1p.org"><img src="../../wikis/images/m1p_logo.png" alt="My First Scientific Paper" height="80px"></a>
<a href="http://fpmi.tilda.ws/algo-tech/"><img src="../../wikis/images/atp-mipt.jpg" alt="ATP MIPT" height="80px"></a></p>

# Contacts 

You can reach developers at the [Telegram user group](https://t.me/mldev_betatest)

# License

The software is licensed under [Apache 2.0 license](LICENSE)
